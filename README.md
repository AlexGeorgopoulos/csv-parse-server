# Csv parse server

## Tech Stack
* NodeJs version 12.17.0+
* Fastify nodejs framework https://www.fastify.io/
* Vue.js, axios and skeleton.css as CDN

## Steps to set up the server and run the app
* Install NodeJs in your machine from here: https://nodejs.org/en/download/package-manager/.
* Run the command: `npm i`.
* Run the command: `npm run start:my-server` (here we pass an env variable for the FILE_LOCATION in order to make it dynamic).

### **Note**: Data fetched-updated via web sockets.