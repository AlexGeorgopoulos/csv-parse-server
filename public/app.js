var app = new Vue({
    el: '#app',
    data() {
        return {
            intervalId: null,
            currentTime: new Date(),
            tableData: [],
            results: []
        }
    },
    methods: {
        setBackgroundColor(status) {
            switch (status) {
                case 'positive':
                    return 'background: lightseagreen; color: white'
                case 'negative':
                    return 'background: firebrick; color: white'
            }
        },
    },
    created() {
        this.intervalId = setInterval(() => this.currentTime = new Date(), 1000);

        const host = window.location.host;
        const socket = new WebSocket(`ws://${host}/data`);

        socket.onmessage = event => {
            const websocketData = JSON.parse(event.data);

            this.tableData = websocketData.tableData;
            this.results = websocketData.results;
        };

        socket.onclose = function (event) {
            if (event.wasClean) {
                console.log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
            } else {
                console.log('[close] Connection died');
            }
        };

        socket.onerror = function (error) {
            console.log(`[error] ${error.message}`);
        };
    },
    beforeDestroy() {
        if (this.intervalId) {
            clearInterval(this.intervalId)
        }
    }
})