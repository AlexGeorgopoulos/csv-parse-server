// @ts-nocheck

const fs = require('fs')
const fastify = require('fastify')({ logger: false })

const path = require('path')
const utils = require('./utils');

sendCSVDataViaSocket = utils.sendCSVDataViaSocket;

fastify.register(require('fastify-websocket'))

const fileLocation = process.env.FILE_LOCATION;

fastify.register(require('fastify-compress'));
fastify.register(require('fastify-static'), {
    root: path.join(__dirname, 'public'),
    prefix: '/public/',
})

fastify.get('/', (request, reply) => {
    reply.sendFile('index.html')
})

fastify.get('/data', { websocket: true }, (connection, req) => {
    sendCSVDataViaSocket(fileLocation, connection.socket)

    fs.watchFile(fileLocation, { interval: 1000 }, () => {
        sendCSVDataViaSocket(fileLocation, connection.socket);
    });
})

// Run the server!
fastify.listen(8060, '0.0.0.0', (err, address) => {
    if (err) {
        fastify.log.error(err)
        process.exit(1)
    }

    fastify.log.info(`server listening on ${address}`)
})
