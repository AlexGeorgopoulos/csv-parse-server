const fs = require('fs')
const parseCsv = require('csv-parse')
const getStream = require('get-stream');

/**
 * @param {string} stringData
 * @param {number} columnNumber
 * @returns {'neutral' | 'positive' | 'negative'}
 */
function _statusGeneratorColumns(stringData, columnNumber) {
    switch (columnNumber) {
        case 7:
        case 13:
            if (Number(stringData) < 0) {
                return 'negative'
            }
            break;
        default:
            return 'neutral'
    }
    return 'neutral'
}

/**
 * @param {number} numberData
 * @returns {'neutral' | 'positive' | 'negative'}
 */
function _statusGeneratorTotals(numberData) {
    if (numberData > 0) {
        return 'positive'
    } else if (numberData < 0) {
        return 'negative'
    }
    return 'neutral'
}

/**
 * @param {string} stringData
 * @param {number} columnNumber
 * @returns {{status: 'neutral' | 'positive' | 'negative', value: string}}
 */
function _formatColumnData(stringData, columnNumber) {
    return {
        status: _statusGeneratorColumns(stringData, columnNumber),
        value: stringData.trim()
    }
}

/**
 * @param {string} filePath
 * @returns {string}
 */
const sendCSVDataViaSocket = async (filePath, socket) => {
    try {
        const parseStream = parseCsv({ delimiter: ',' });
        const output = await getStream.array(fs.createReadStream(filePath).pipe(parseStream));

        const trimmedTableData = output.map(stringArray =>
            stringArray.map(_formatColumnData))

        const lastRowWithTotals = trimmedTableData[trimmedTableData.length - 1]
        const totalPNL = Number(Number(lastRowWithTotals[7].value).toFixed(2))
        const totalTurnover = Number(Number(lastRowWithTotals[13].value).toFixed(2))
        const totalPNLPerEuro = Number((totalPNL * 10000 / totalTurnover).toFixed(3))

        socket.send(JSON.stringify({
            tableData: trimmedTableData,
            results: [
                {
                    value: totalPNL,
                    status: _statusGeneratorTotals(totalPNL),
                    text: `Total PNL: ${totalPNL} €`
                },
                {
                    value: totalTurnover,
                    status: _statusGeneratorTotals(totalTurnover),
                    text: `Total Turnover: ${totalTurnover} €`
                },
                {
                    value: totalPNLPerEuro,
                    status: _statusGeneratorTotals(totalPNLPerEuro),
                    text: `PNL/EUR: ${totalPNLPerEuro} BPS`
                }
            ]
        }))
    } catch(error) {
        throw error;
    }
}

module.exports = {
    sendCSVDataViaSocket
}